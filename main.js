import axiod from 'https://deno.land/x/axiod@0.26.1/mod.ts';

import {
    apiKey,
    memberId,
    eventTypes,
    jsonPath
} from './config.js';

const client = axiod.create({
    baseURL: 'https://api.betaseries.com',
    headers: {
        'X-BetaSeries-Key': apiKey,
        'User-Agent': 'betaseries-timeline-export'
    }
});

let events, lastEvents;

try {
    events = JSON.parse(await Deno.readTextFile(jsonPath));
}
catch(error){
    if(error.code === 'ENOENT')
        events = [];
    else {
        console.error(error);
        Deno.exit(1);
    }
}

try {
    do {
        ({
            data: {
                events: lastEvents
            }
        } = await client.get(
            '/timeline/member',
            {
                params: {
                    'id': memberId,
                    'nbpp': 1,
                    'types': eventTypes.join(','),
                    ...events && events.length ? {
                        'since_id': events.slice(-1)[0]['id']
                    } : {}
                }
            }
        ));
        if(lastEvents && lastEvents.length){
            const duplicateEvent = lastEvents.find(newEvent => events.find(existingEvent => newEvent['id'] === existingEvent['id']));
            if(duplicateEvent){
                console.error(`Duplicate event :\n${JSON.stringify(duplicateEvent, null, 4)}`);
                Deno.exit(1);
            }
            else {
                events.push(...lastEvents);
                await Deno.writeTextFile(jsonPath, JSON.stringify(events, null, 4));
                console.log(`Event count : ${events.length} | Last event date : ${JSON.stringify(events.slice(-1)[0]['date'])}`);
            }
        }
    } while(lastEvents && lastEvents.length);
}
catch(error){
    console.error(error);
    Deno.exit(1);
}